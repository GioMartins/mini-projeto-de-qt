#include "controler.h"

Controler::Controler() {
    this->GLaDOS = this->Wheatley = false;
    this->Gideon = true;
}

bool Controler::getGLaDOS() const {
    return this->GLaDOS;
}

bool Controler::getGideon() const {
    return this->Gideon;
}

bool Controler::getWheatley() const {
    return this->Wheatley;
}

void Controler::setGlados(bool change) {
    if(getGLaDOS() != change) {
        this->GLaDOS = change;
        emit this->GLaDOSChanged(change);
    }
}

void Controler::setGideon(bool change) {
    if(getGideon() != change) {
        this->Gideon = change;
        emit this->GideonChanged(change);
    }
}

void Controler::setWheatley(bool change) {
    if(getWheatley() != change) {
        this->Wheatley = change;
        emit this->WheatleyChanged(change);
    }
}

void Controler::plasticReady(bool ready) {
    if(ready) {
        stateOne();
    } else {
        qDebug() << "The plastic isn't ready.";
    }
}

void Controler::stateOne() {
    setGlados(true);
    qDebug() << "Start process.";
    QTimer::singleShot(5000, this, SLOT(stateTwo()));
}

void Controler::stateTwo() {
    setGlados(false);
    setWheatley(true);
    qDebug() << "The plastic has been heated.";
    qDebug() << "Expanding the bottle.";
    QTimer::singleShot(500, this, SLOT(stateThree()));
}

void Controler::stateThree() {
    setGideon(false);
    qDebug() << "Bottle expanded.";
    QTimer::singleShot(4000, this, SLOT(stateOne()));
    qDebug() << "Another Bottle finished.";
    setGideon(true);
    setWheatley(false);
}

