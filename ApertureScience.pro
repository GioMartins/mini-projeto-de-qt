#-------------------------------------------------
#
# Project created by QtCreator 2015-05-20T09:57:56
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = ApertureScience
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    controler.cpp

HEADERS += \
    controler.h
