#ifndef CONTROLER_H
#define CONTROLER_H
#include <QObject>
#include <QTimer>
#include <QDebug>

class Controler : public QObject{
    Q_OBJECT
    Q_PROPERTY(bool GLaDOS READ getGLaDOS WRITE setGlados NOTIFY GLaDOSChanged)
    Q_PROPERTY(bool Gideon READ getGideon WRITE setGideon NOTIFY GideonChanged)
    Q_PROPERTY(bool Wheatley READ getWheatley WRITE setWheatley NOTIFY WheatleyChanged)
public:
    Controler();
    bool getGLaDOS() const;
    bool getGideon() const;
    bool getWheatley() const;
signals:
    void GLaDOSChanged(bool GLaDOS);
    void GideonChanged(bool Gideon);
    void WheatleyChanged(bool Wheatley);
public slots:
    void plasticReady(bool ready);
    void stateOne();
    void stateTwo();
    void stateThree();
private slots:
    void setGlados(bool change);
    void setGideon(bool change);
    void setWheatley(bool change);
private:
    bool GLaDOS;
    bool Gideon;
    bool Wheatley;
};

#endif // CONTROLER_H
